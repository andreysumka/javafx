package tips;


import java.net.URL;
import java.util.ResourceBundle;

import arithmometr.animation.Shake;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ComboBox<String> comboBox;

    @FXML
    private TextField chequeRub;

    @FXML
    private Button buttonClEAR;

    @FXML
    private Button buttonNot;

    @FXML
    private Button button5;

    @FXML
    private Button button10;

    @FXML
    private TextField tip;

    @FXML
    private TextField ruble;

    @FXML
    private TextField tipCurrency;

    @FXML
    private TextField tipRubles;

    @FXML
    private TextField cheque;

    @FXML
    private Button buttonCalculate;

    double input(){
        return  Double.parseDouble(tip.getText());
    }

    @FXML
    void calculate(ActionEvent event) {
        if (ruble.getText().isEmpty() && tipCurrency.getText().isEmpty() && tipRubles.getText().isEmpty()) {
            Shake rubAnim = new Shake(ruble);
            Shake tipCurAnim = new Shake(tipCurrency);
            Shake tipRubAnim = new Shake(tipRubles);
            rubAnim.playAnimation();
            tipCurAnim.playAnimation();
            tipRubAnim.playAnimation();
            return;
        }

        double tipResult = Double.parseDouble(tip.getText()) + Double.parseDouble(tipCurrency.getText());
        double rubResult = Double.parseDouble(ruble.getText()) + Double.parseDouble(tipRubles.getText());
        cheque.setText(String.valueOf(tipResult));
        chequeRub.setText(String.valueOf(rubResult));
    }

    @FXML
    void clear(ActionEvent event) {
        tip.clear();
        ruble.clear();
        tipRubles.clear();
        tipCurrency.clear();
        chequeRub.clear();
        cheque.clear();
    }

    @FXML
    void leave10(ActionEvent event) {
        String box1 = comboBox.getValue();

        if (tip.getText().isEmpty()) {
            Shake tipAnim = new Shake(tip);
            tipAnim.playAnimation();
            return;
        }

        if(box1.equals("Евро")){
            double euro = input();
            double rub = euro * 80;
            double tipsInCurrency = euro / 100 * 10;
            double tipsInRuble = rub / 100 * 10;
            ruble.setText(String.valueOf(rub));
            tipCurrency.setText(String.valueOf(tipsInCurrency));
            tipRubles.setText(String.valueOf(tipsInRuble));
        }
        if(box1.equals("Доллар")){
            double dollar = input();
            double rub = dollar * 72;
            double tipsInCurrency = dollar / 100 * 10;
            double tipsInRuble = rub / 100 * 10;
            ruble.setText(String.valueOf(rub));
            tipCurrency.setText(String.valueOf(tipsInCurrency));
            tipRubles.setText(String.valueOf(tipsInRuble));
        }
    }

    @FXML
    void leave5(ActionEvent event) {
        String box1 = comboBox.getValue();

        if (tip.getText().isEmpty()) {
            Shake tipAnim = new Shake(tip);
            tipAnim.playAnimation();
            return;
        }

        if(box1.equals("Euro")){
            double euro = input();
            double rub = euro * 80;
            double tipsInCurrency = euro / 100 * 5;
            double tipsInRuble = rub / 100 * 5;
            ruble.setText(String.valueOf(rub));
            tipCurrency.setText(String.valueOf(tipsInCurrency));
            tipRubles.setText(String.valueOf(tipsInRuble));
        }
        if(box1.equals("Dollar")){
            double dollar = input();
            double rub = dollar * 72;
            double tipsInCurrency = dollar / 100 * 5;
            double tipsInRuble = rub / 100 * 5;
            ruble.setText(String.valueOf(rub));
            tipCurrency.setText(String.valueOf(tipsInCurrency));
            tipRubles.setText(String.valueOf(tipsInRuble));
        }
    }

    @FXML
    void notLeave(ActionEvent event) {
        String box1 = comboBox.getValue();

        if (tip.getText().isEmpty()) {
            Shake tipAnim = new Shake(tip);
            tipAnim.playAnimation();
            return;
        }

        if(box1.equals("Euro")){
            double euro = input();
            double rub = euro * 80;
            ruble.setText(String.valueOf(rub));
            tipCurrency.setText(String.valueOf(0));
            tipRubles.setText(String.valueOf(0));
        }
        if(box1.equals("Dollar")){
            double dollar = input();
            double rub = dollar * 72;
            ruble.setText(String.valueOf(rub));
            tipCurrency.setText(String.valueOf(0));
            tipRubles.setText(String.valueOf(0));
        }
    }

    @FXML
    void initialize() {

    }
}
