package converter;

import java.net.URL;
import java.util.ResourceBundle;

import arithmometr.animation.Shake;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private TextField mmField;

    @FXML
    private TextField smField;

    @FXML
    private TextField dmField;

    @FXML
    private TextField mField;

    @FXML
    private TextField kmField;

    @FXML
    private Button calculing5;

    @FXML
    private Button calculing1;

    @FXML
    private Button calculing2;

    @FXML
    private Button calculing3;

    @FXML
    private Button calculing4;

    @FXML
    private Button сlearButton;

    @FXML
    private TextField mgField;

    @FXML
    private TextField kgField;

    @FXML
    private TextField gField;

    @FXML
    private Button calculingMg;

    @FXML
    private Button calculingKg;

    @FXML
    private Button calculingG;

    @FXML
    private Button сlearButton1;

    @FXML
    void calculateDm(ActionEvent event) {
        if (dmField.getText().isEmpty()) {
            Shake firstOperandAnim = new Shake(mField);
            Shake secondOperandAnim = new Shake(dmField);
            Shake thirdOperandAnim = new Shake(mmField);
            Shake fourthOperandAnim = new Shake(smField);
            Shake fiveOperandAnim = new Shake(kmField);
            firstOperandAnim.playAnimation();
            secondOperandAnim.playAnimation();
            thirdOperandAnim.playAnimation();
            fourthOperandAnim.playAnimation();
            fiveOperandAnim.playAnimation();
            return;
        }
        double dm = Double.parseDouble((dmField.getText()));

        double fromDmInMm = dm * 100;
        mmField.setText(String.valueOf(fromDmInMm));
        double fromDmInSm = dm * 10;
        smField.setText(String.valueOf(fromDmInSm));
        double fromDmInM = dm / 10;
        mField.setText(String.valueOf(fromDmInM));
        double fromDmInKm = dm / 10000;
        kmField.setText(String.valueOf(fromDmInKm));
    }

    @FXML
    void calculateKm(ActionEvent event) {
        if (kmField.getText().isEmpty()) {
            Shake firstOperandAnim = new Shake(mField);
            Shake secondOperandAnim = new Shake(dmField);
            Shake thirdOperandAnim = new Shake(mmField);
            Shake fourthOperandAnim = new Shake(smField);
            Shake fiveOperandAnim = new Shake(kmField);
            firstOperandAnim.playAnimation();
            secondOperandAnim.playAnimation();
            thirdOperandAnim.playAnimation();
            fourthOperandAnim.playAnimation();
            fiveOperandAnim.playAnimation();
            return;
        }
        double km = Double.parseDouble(kmField.getText());

        double fromKmInMm = km * 1000000;
        mmField.setText(String.valueOf(fromKmInMm));
        double fromKmInSm = km * 100000;
        smField.setText(String.valueOf(fromKmInSm));
        double fromKmInDm = km * 10000;
        dmField.setText(String.valueOf(fromKmInDm));
        double fromKmInM = km * 1000;
        mField.setText(String.valueOf(fromKmInM));
    }

    @FXML
    void calculateM(ActionEvent event) {
        if (mField.getText().isEmpty()) {
            Shake firstOperandAnim = new Shake(mField);
            Shake secondOperandAnim = new Shake(dmField);
            Shake thirdOperandAnim = new Shake(mmField);
            Shake fourthOperandAnim = new Shake(smField);
            Shake fiveOperandAnim = new Shake(kmField);
            firstOperandAnim.playAnimation();
            secondOperandAnim.playAnimation();
            thirdOperandAnim.playAnimation();
            fourthOperandAnim.playAnimation();
            fiveOperandAnim.playAnimation();
            return;
        }
        double m = Double.parseDouble(mField.getText());

        double fromMInMm = m * 1000;
        mmField.setText(String.valueOf(fromMInMm));
        double fromMInSm = m * 100;
        smField.setText(String.valueOf(fromMInSm));
        double fromMInDm = m * 10;
        dmField.setText(String.valueOf(fromMInDm));
        double fromMInKm = m / 1000;
        kmField.setText(String.valueOf(fromMInKm));
    }

    @FXML
    void calculateMm(ActionEvent event) {
        if (mmField.getText().isEmpty()) {
            Shake firstOperandAnim = new Shake(mField);
            Shake secondOperandAnim = new Shake(dmField);
            Shake thirdOperandAnim = new Shake(mmField);
            Shake fourthOperandAnim = new Shake(smField);
            Shake fiveOperandAnim = new Shake(kmField);
            firstOperandAnim.playAnimation();
            secondOperandAnim.playAnimation();
            thirdOperandAnim.playAnimation();
            fourthOperandAnim.playAnimation();
            fiveOperandAnim.playAnimation();
            return;
        }
        double mm = Double.parseDouble(mmField.getText());

        double fromMmInSm = mm / 10;
        smField.setText(String.valueOf(fromMmInSm));
        double fromMmInDm = mm / 100;
        dmField.setText(String.valueOf(fromMmInDm));
        double fromMmInM = mm / 1000;
        mField.setText(String.valueOf(fromMmInM));
        double fromMmInKm = mm / 1000000;
        kmField.setText(String.valueOf(fromMmInKm));
    }

    @FXML
    void calculateSm(ActionEvent event) {
        if (smField.getText().isEmpty()) {
            Shake firstOperandAnim = new Shake(mField);
            Shake secondOperandAnim = new Shake(dmField);
            Shake thirdOperandAnim = new Shake(mmField);
            Shake fourthOperandAnim = new Shake(smField);
            Shake fiveOperandAnim = new Shake(kmField);
            firstOperandAnim.playAnimation();
            secondOperandAnim.playAnimation();
            thirdOperandAnim.playAnimation();
            fourthOperandAnim.playAnimation();
            fiveOperandAnim.playAnimation();
            return;
        }
        double sm = Double.parseDouble(smField.getText());

        double fromSmInMm = sm * 10;
        mmField.setText(String.valueOf(fromSmInMm));
        double fromSmInDm = sm / 10;
        dmField.setText(String.valueOf(fromSmInDm));
        double fromSmInM = sm / 100;
        mField.setText((String.valueOf(fromSmInM)));
        double fromSmInKm = sm / 100000;
        kmField.setText(String.valueOf(fromSmInKm));
    }

    @FXML
    void clear(ActionEvent event) {
        mmField.clear();
        smField.clear();
        dmField.clear();
        mField.clear();
        kmField.clear();
    }

    @FXML
    void calculateMg(ActionEvent event) {
        if (mgField.getText().isEmpty()) {
            Shake firstOperandAnimWg = new Shake(mgField);
            Shake secondOperandAnimWg = new Shake(gField);
            Shake thirdOperandAnimWg = new Shake(kgField);
            firstOperandAnimWg.playAnimation();
            secondOperandAnimWg.playAnimation();
            thirdOperandAnimWg.playAnimation();
            return;
        }
        double mg = Double.parseDouble(mgField.getText());

        double fromMgInG = mg / 1000;
        gField.setText(String.valueOf(fromMgInG));
        double fromMgInKg = mg / 1000000;
        kgField.setText(String.valueOf(fromMgInKg));
    }

    @FXML
    void calculateG(ActionEvent event) {
        if (gField.getText().isEmpty()) {
            Shake firstOperandAnimWg = new Shake(mgField);
            Shake secondOperandAnimWg = new Shake(gField);
            Shake thirdOperandAnimWg = new Shake(kgField);
            firstOperandAnimWg.playAnimation();
            secondOperandAnimWg.playAnimation();
            thirdOperandAnimWg.playAnimation();
            return;
        }
        double g = Double.parseDouble(gField.getText());

        double fromGInMg = g * 1000;
        mgField.setText(String.valueOf(fromGInMg));
        double fromGInKg = g / 1000;
        kgField.setText(String.valueOf(fromGInKg));
    }

    @FXML
    void calculateKg(ActionEvent event) {
        if (kgField.getText().isEmpty()) {
            Shake firstOperandAnimWg = new Shake(mgField);
            Shake secondOperandAnimWg = new Shake(gField);
            Shake thirdOperandAnimWg = new Shake(kgField);
            firstOperandAnimWg.playAnimation();
            secondOperandAnimWg.playAnimation();
            thirdOperandAnimWg.playAnimation();
            return;
        }
        double kg = Double.parseDouble(kgField.getText());

        double fromKgInMg = kg * 1000000;
        mgField.setText(String.valueOf(fromKgInMg));
        double fromKgInG = kg * 1000;
        gField.setText(String.valueOf(fromKgInG));
    }

    @FXML
    void clearWg(ActionEvent event) {
        mgField.clear();
        gField.clear();
        kgField.clear();
    }

    @FXML
    void initialize() {

    }
}

