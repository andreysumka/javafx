package sample;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;

public class Controller2 {

    @FXML
    private ResourceBundle resources;

    @FXML
    private URL location;

    @FXML
    private ImageView clown;

    @FXML
    void initialize() {
        assert clown != null : "fx:id=\"clown\" was not injected: check your FXML file 'sample2.fxml'.";

    }
}